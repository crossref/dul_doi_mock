# DOI DUL Staging

# OpenJDK because of HTTP-kit compatibility.
FROM clojure:openjdk-8-lein
MAINTAINER Joe Wass jwass@crossref.org

COPY src /usr/src/app/src
COPY resources /usr/src/app/resources
COPY project.clj /usr/src/app/project.clj

WORKDIR /usr/src/app

RUN lein deps && lein compile
CMD lein run
